package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;

public class ThreadDAOTests {

    private JdbcTemplate mockJdbc;
    private ThreadDAO mockThread;

    private int id;
    private int limit;
    private int since;

    @BeforeEach
    void treeSortTest() {
        mockJdbc = mock(JdbcTemplate.class);
        mockThread = new ThreadDAO(mockJdbc);
        id = 25;
        limit = 15;
        since = 13;
    }

    @Test
    void TreeSortTest1() {
        ThreadDAO.treeSort(id, limit, since, null);
        String queryString = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;";
        Mockito.verify(mockJdbc).query(Mockito.eq(queryString), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt(), Mockito.anyInt(),
                Mockito.anyInt());
    }

    @Test
    void TreeSortTest2() {
        ThreadDAO.treeSort(id, limit, since, true);
        String queryString = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;";
        Mockito.verify(mockJdbc).query(Mockito.eq(queryString), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt(), Mockito.anyInt(),
                Mockito.anyInt());
    }
}