package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ForumDAOTests {

    private JdbcTemplate mockJdbc;
    private ForumDAO forum;
    private String since;
    private String slug;
    private int limit;
    private UserDAO.UserMapper USER_MAPPER;

    @BeforeEach
    void userListTest() {
        mockJdbc = mock(JdbcTemplate.class);
        forum = new ForumDAO(mockJdbc);
        since = "10.10.2022";
        slug = "slug";
        limit = 15;
        USER_MAPPER = new UserDAO.UserMapper();
    }

    @Test
    void UserListTest1() {
        ForumDAO.UserList(slug, null, null, null);
        String queryString = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;";
        verify(mockJdbc).query(Mockito.eq(queryString), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void UserListTest2() {
        ForumDAO.UserList(slug, null, since, null);
        String queryString = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;";
        verify(mockJdbc).query(Mockito.eq(queryString), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void UserListTest3() {
        ForumDAO.UserList(slug, limit, since, true);
        String queryString = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;";
        verify(mockJdbc).query(Mockito.eq(queryString), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}
