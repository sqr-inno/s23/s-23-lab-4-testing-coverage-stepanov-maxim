package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;

import static org.mockito.Mockito.mock;


public class PostDAOTests {

        private JdbcTemplate mockJdbc;
        private PostDAO mockPost;

        private Post post;
        private int id = 42;
        private String defaultString = "SELECT * FROM \"posts\" WHERE id=? LIMIT 1;";

        private String msg = "message";
        private String msg2 = "message2";

        private String author = "author";
        private String author2 = "author2";

        private String forum = "forum";
        private String forum2 = "forum2";
        
        private Timestamp date = new Timestamp(1234567);
        private Timestamp date2 = new Timestamp(12345678);

        //prepare for testing
        @BeforeEach
        void setPostTest() {
                mockJdbc = mock(JdbcTemplate.class);
                mockPost = new PostDAO(mockJdbc);
                post = new Post(author, date, forum, msg, 0, 0, false);
                Mockito.when(mockJdbc.queryForObject(Mockito.eq(defaultString), Mockito.any(PostDAO.PostMapper.class),
                                Mockito.eq(id))).thenReturn(post);
        }

        //same post
        @Test
        void SetPostTest1() {
                PostDAO.setPost(id, post);
                Mockito.verify(mockJdbc).queryForObject(Mockito.eq(defaultString), Mockito.any(PostDAO.PostMapper.class),
                                Mockito.eq(id));
        }

        //only created() doesn't coincide
        @Test
        void SetPostTest2() {
                Post post2 = new Post(author, date2, forum, msg, 0, 0, false);
                PostDAO.setPost(id, post2);
                String queryString = "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
                Mockito.verify(mockJdbc).update(Mockito.eq(queryString), Mockito.any(Timestamp.class), Mockito.anyInt());
        }

        //date and message are different
        @Test
        void SetPostTest3() {
                Post post2 = new Post(author, date2, forum, msg2, 0, 0, false);
                PostDAO.setPost(id, post2);
                String queryString = "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
                Mockito.verify(mockJdbc).update(Mockito.eq(queryString), Mockito.anyString(), Mockito.any(Timestamp.class), Mockito.anyInt());
        }

        //author, date and message are different
        @Test
        void SetPostTest4() {
                Post post2 = new Post(author2, date2, forum, msg2, 0, 0, false);
                PostDAO.setPost(id, post2);
                String queryString = "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
                Mockito.verify(mockJdbc).update(Mockito.eq(queryString), Mockito.anyString(), Mockito.anyString(), Mockito.any(Timestamp.class), Mockito.anyInt());
        }
}