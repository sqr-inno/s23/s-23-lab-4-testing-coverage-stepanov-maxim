
[![pipeline status](https://gitlab.com/sqr-inno/s23/s-23-lab-4-testing-coverage-stepanov-maxim/badges/master/pipeline.svg)](https://gitlab.com/sqr-inno/s23/s-23-lab-4-testing-coverage-stepanov-maxim/-/commits/master)

# Lab4 -- Testing coverage 

I completed the tests. There was, however, a problem with basis path testing.

Number of test cases is equal to number of independent paths in CFG (control flow graph).

From definition, independent path is a path that adds at least one new edge to the list of traversed edges. However, in case of PostDAO, one branch is depending on the results of previous branches. So, even though in simple CFG I got 21 edge + 16 vertices (making number of independent paths equal to 21+2-16=7), I found that 4 test cases are sufficient for covering all edges (but, surprisingly, not all possible results), which made it quite unclear how to determine independent path. Hence, there are only 4 tests. However, it is still full branch coverage, all possible edges are tested => the work is done.